from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _
from hashlib import md5
import os
from urllib.parse import urlencode

AVATAR_WIDTH, AVATAR_HEIGHT = getattr(settings, 'DEFAULT_AVATAR_SIZE', (74, 74))


class UserManager(BaseUserManager):
    def _create_user(self, phone, password, email, is_superuser, **extra_fields):
        now = timezone.now()
        if not phone:
            raise ValueError('A phone number must be supplied.')
        email = self.normalize_email(email)
        user = self.model(phone=phone,
                          email=email,
                          is_active=True,
                          is_superuser=is_superuser,
                          last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, password, email, **extra_fields):
        return self._create_user(phone, password, email, False, **extra_fields)

    def create_superuser(self, phone, password, email, **extra_fields):
        return self._create_user(phone, password, email, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    NOT_VERIFIED, PHONE_VERIFIED, EMAIL_VERIFIED, VERIFIED = 0, 1, 2, 3
    VALIDATION_STATUS = (
        (NOT_VERIFIED, _('Not verified')),
        (PHONE_VERIFIED, _('Phone number verified')),
        (EMAIL_VERIFIED, _('Email verified')),
        (VERIFIED, _('Verified')),
    )

    full_name = models.CharField(_('Full name'), max_length=100, blank=True)
    email = models.EmailField(_('Email address'), max_length=254, unique=True)
    phone = models.CharField(_('Phone number'), max_length=20, unique=True,
        help_text=_('Phone number in international format, without the leading "+".'))
    is_active = models.BooleanField(_('Active'), default=True,
        help_text=_('Specifies this user as active. De-select this, rather than deleting the account.'))
    date_joined = models.DateTimeField(_('Date joined'), default=timezone.now)
    status = models.PositiveSmallIntegerField(_('Validation status'), default=NOT_VERIFIED)

    objects = UserManager()

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        return self.email

    @property
    def is_staff(self):
        return self.is_superuser

    @property
    def verified(self):
        return self.status == self.VERIFIED

    def get_absolute_url(self):
        return '/users/%s/' % (self.phone)

    def get_full_name(self):
        return self.full_name

    def get_short_name(self):
        return self.full_name.split()[0] if self.full_name else ''

    def get_initials(self):
        return ''.join(name[0] for name in self.full_name.split()).upper()

    def get_gravatar_url(self):
        return "https://www.gravatar.com/avatar/{hash}?{query}".format(
            hash=md5(force_bytes(self.email)).hexdigest(), 
            query=urlencode({'d': settings.DEFAULT_AVATAR_URL, 's': str(AVATAR_WIDTH)})
        )
