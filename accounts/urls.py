from django.conf.urls import include, url
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import views as auth_views
from .views import register_view, verify_email

app_name = 'accounts'

urlpatterns = [
    url(r'^logout$', auth_views.logout, name='logout'),
    url(r'^reset-password$', auth_views.password_reset, 
        {'email_template_name': 'registration/password_reset_email.txt', 
         'html_email_template_name': 'registration/password_reset_email.html',
         'post_reset_redirect': reverse_lazy('accounts:reset_requested')},
        name='reset_password'),
    url(r'^reset-password/requested$', auth_views.password_reset_done, name='reset_requested'),
    url(r'^reset-password/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})$', 
        auth_views.password_reset_confirm, {'post_reset_redirect': reverse_lazy('accounts:reset_password_complete')},
        name='confirm_reset_password'),
    url(r'^reset-password/complete$', auth_views.password_reset_complete, name='reset_password_complete'),
    url(r'^login$', auth_views.login, name='login'),
    url(r'^register$', register_view, name='register'),
    url(r'verify/(?P<fldb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})$', 
        verify_email, name='verify_email'),
]
