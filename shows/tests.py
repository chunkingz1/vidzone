from django.contrib.auth import get_user_model
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.test import SimpleTestCase, TestCase
from channels.test import ChannelTestCase
from collections import defaultdict
from datetime import datetime
from decimal import Decimal
from unittest import mock
from .models import Channel, Show, ShowPayment
from .consumers import payment_received, show_invites
import channels
import pytz

User = get_user_model()

DATE = datetime(2000, 4, 13, 0, 0, 0, 0, pytz.UTC)


class ShowTestCase(ChannelTestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create_user('2348022221000', 'pass', 'a@x.net', full_name='Rocky Road')
        publisher = Channel.objects.create(owner=user, name='We Stream Good', slug='we-stream')
        show = Show.objects.create(channel=publisher, stream_key='first', title='Second Stream', mode=Show.PAID, cost=Decimal(200))
        ShowPayment.objects.create(payer=user, payee=show.channel, show=show, amount=int(show.cost * 100))

    @mock.patch('django.utils.timezone.now', side_effect=lambda: DATE)
    @mock.patch('paystackapi.paystack.Transaction.initialize',
                side_effect=lambda **kwds: {'status': True, 'data': {'authorization_url': '/to-paystack'}})
    def test_show_payment_passes_params_correctly(self, transaction_init, now):
        show = Show.objects.first()
        self.client.login(username='2348022221000', password='pass')
        res = self.client.post(reverse('shows:payment', kwargs={'show_name': show.stream_name}))
        transaction_init.assert_called_once()
        transaction_init.assert_called_with(email='a@x.net', amount=int(show.cost * 100), callback_url=mock.ANY, reference=mock.ANY)
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, '/to-paystack')

    @mock.patch('paystackapi.paystack.Transaction.verify',
                side_effect=lambda ref: {
                    'status': True,
                    'data': defaultdict(str, 
                        amount=20000,
                        transaction_date=DATE,
                        authorization=defaultdict(str))
                })
    def test_successful_payment_should_redirect(self, transaction_verify):
        payment = ShowPayment.objects.first()
        res = self.client.get(f"{reverse('shows:verify')}?reference={payment.code}")
        transaction_verify.assert_called_once()
        transaction_verify.assert_called_with(payment.code)
        self.assertEqual(res.status_code, 302)

    def test_handle_payment_received(self):
        payment = ShowPayment.objects.first()
        content = {'payment': payment.pk}
        channels.Channel('plans.payment_received').send(content)
        payment_received(self.get_next_message('plans.payment_received', require=True))
        self.assertEqual(len(mail.outbox), 1)
        self.assertTrue(isinstance(mail.outbox[0], EmailMultiAlternatives), 'Email should be multipart/alternative.')
        self.assertTrue(payment.payer.email in mail.outbox[0].to, 'Recipient not found.')
        self.assertEqual(len(mail.outbox[0].alternatives), 1)

    def test_handle_send_invite(self):
        payment = ShowPayment.objects.first()
        content = {'emails': payment.payer.email, 'base_url': 'http://streams.com', 'show': payment.show.pk}
        channels.Channel('show.invites').send(content)
        show_invites(self.get_next_message('show.invites', require=True))
        self.assertEqual(len(mail.outbox), 1)
        self.assertTrue(isinstance(mail.outbox[0], EmailMultiAlternatives), 'Email should be multipart/alternative.')
        self.assertTrue(payment.payer.email in mail.outbox[0].to, 'Recipient not found.')
        self.assertEqual(len(mail.outbox[0].alternatives), 1)
